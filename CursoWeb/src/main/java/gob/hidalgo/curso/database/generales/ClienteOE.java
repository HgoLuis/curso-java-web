package gob.hidalgo.curso.database.generales;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonIgnore;

import gob.hidalgo.curso.utils.EntityObject;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Alias("ClienteOE")
public class ClienteOE extends EntityObject implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	@Pattern (regexp="^$|^[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$", message = "Favor de anotar un CURP v�lido") 
	private String curp;
	
	@NotBlank (message="Favor de anotar el Nombre")
	private String nombre;
	
	@NotBlank (message="Favor de anotar el Primer Apellido")
	private String primerApellido;
	
	@NotBlank (message="Favor de anotar el Segundo Apellido")
	private String segundoApellido;
	
	@NotNull (message="Favor de seleccionar la Fecha de Nacimiento")
	@PastOrPresent (message="Favor de seleccionar una fecha de nacimiento valida")
	private LocalDate fechaNacimiento;
	
	@Email (message="Favor de anotar una Mail v�lido")
	private String mail;
	
	@NotBlank (message="Favor de anotar el Celular")
	private String celular;
	
	
	private List<OrdenPagoEO> ordenesPago;
	
	@JsonIgnore 
	private String fechaNacimientoT;
	
	public String getFechaNacimientoT() { //formato a la fecha de nacimiento a dd/MM/yyyy
		if(fechaNacimientoT == null) {
			if (fechaNacimiento!= null) {
			    fechaNacimientoT = fechaNacimiento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			} else {
				fechaNacimientoT="";
			}
		}
		return fechaNacimientoT;
	}
	
}
